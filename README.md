## Sistema de finanzas (Laravel)

Sistema de finanzas en Php (Laravel 5.4)

- Lenguaje: PHP.
- Framework: Laravel.
- Base de datos: Mysql.


## Info

USER: admin@admin.com  
PASS: 123456

## Instalación 

- [Composer install](https://getcomposer.org/).
- composer global require "laravel/installer"
- composer install
- [Importar Base de datos](https://github.com/yond1994/finanzas/blob/master/db.sql).
- cambiar credenciales por las tuyas en archivo .env 

## Ejecutar proyecto 

 php artisan serve

## Datos extra


URL ADMIN:  http://localhost:8000

USER: admin@admin.com  
PASS: 123456


